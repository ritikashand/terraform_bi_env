resource "aws_vpc_peering_connection" "rdgw_tableau_peer" {
  peer_owner_id = "${var.peer_owner_id}"
  peer_vpc_id   = "${var.vpc_peer_id}"
  vpc_id        = "${var.vpc_id}"
  peer_region   = "${var.peer_region}"
  #peer_region   = "us-east-1"
  tags = "${merge(var.tags,map("Name","VPC_Peer_${var.Name}"))}"
}
