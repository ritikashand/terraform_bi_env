<powershell>
# Providing unrestricted access to execution policies
Set-ExecutionPolicy -ExecutionPolicy unrestricted

# Creating a file for storing logs/errors
$logfile = “c:\RDGWdeploy.log”

#Creating a file to check for the loops
$statusfile = “c:\RDGWStatus.log"

#Function to write to the log file
Function Write-Log
{
  Param ([string]$logstring)
  Add-Content $logfile -Value $logstring
}

#Function to write to the status file
Function Write-Status
{
  Param ([string]$logstring)
  Add-Content $statusfile -Value $logstring
}

#Function to change the status file
Function Replace-Status
{
  Param ([string]$logstring)
  Set-Content $statusfile -Value $logstring
}

If(-NOT (Test-Path $statusfile)){
  New-Item $statusfile -ItemType file
  Write-Status “0"

}

$Value = Get-Content $statusfile

Switch ($Value) {

  # First case: Script is running for the first time,
  # Check if file is empty/not created
  # Install newest aws updates
  # Set new dns servers and update computer name

  '0' {

    Write-Log “Begin script run: $(Get-Date)”

    #get the aws url
    $AWSPowerShellModuleSourceURL = “${aws_toolkit}”
    Write-Log “AWSPowershellURL: $AWSPowerShellModuleSourceURL”

    #set Destination Folder to store the download
    $DestinationFolder = "C:\Users\Administrator\Downloads"
    #$DestinationFolder = "$ENV:homedrive\$ENV:homepath\Downloads"
    Write-Log “DestinationFolder: $DestinationFolder”

    #Set file
    $MSIfile = “$DestinationFolder\AWSToolsAndSDKForNet.msi”
    Write-Log “MSIfile: $MSIfile”
    Write-Log "Current Error : $error"

    # Invoke request
    Invoke-WebRequest -Uri $AWSPowerShellModuleSourceURL -OutFile $MSIfile -ErrorAction STOP 5>>$logfile
    $Arguments = “/i $MSIfile /qb /norestart” 5>>$logfile
    Write-Log "Current Error : $error"
    Write-Log “arguments: $Arguments”

    Write-Log "Current Error : $error"

    #Install
    Start-Process -FilePath "msiexec.exe" -ArgumentList $Arguments -Wait -PassThru 5>>$logfile
    Write-Log "Current Error : $error"
    Write-Log “End of AWS download: $(Get-Date)”

    # set dns servers
    # $dns1 = primary dns server (130.6.227.117)
    # $dns2 = 130.6.227.170

    $dns1 = “${dns1}”
    Write-Log “set dns1 variable: $dns1”

    $dns2 = “${dns2}”
    Write-Log “set dns2 variable: $dns2”

    # Set dns if value is ethernet 3
    Set-DnsClientServerAddress -InterfaceAlias “Ethernet 3” -ServerAddresses (“$dns1”,”$dns2”)
    Write-Log “set for ethernet 3 : $(Get-Date)”

    # Set dns if value is ethernet
    Set-DnsClientServerAddress -InterfaceAlias “Ethernet” -ServerAddresses (“$dns1”,”$dns2”)
    Write-Log “set for ethernet : $(Get-Date)”

    # Set dns if value is ethernet 4
    Set-DnsClientServerAddress -InterfaceAlias “Ethernet 4” -ServerAddresses (“$dns1”,”$dns2”)
    Write-Log “set for ethernet 4 : $(Get-Date)”


    #Give the computer a unique name, can only include '-' along with numbers
    # and alphabets. Can not be longer than 15 characters

    $newcomputername = “RDGW-BI-Env-${name}”
    Rename-Computer -NewName $newcomputername
    Write-Log “set computer name : $newcomputername”

    #Write to status file with value 1
    Replace-Status “1"

    #Reboot
    Restart-Computer

  }

  #Case 2: If the file has value 1 ( has run once) then join the domain
  # using username and password from secrets manager

  '1'{

    # join a domain using domain admin credentials
    $domain = “${domain}”
    Write-Log “set domain : $domain”

    # Get-Credentials will prompt for login info or...
    # Set the ARN of the secret set we want to pull
    $SecretARN = “${secret}”
    Write-Log “secret ARN : $SecretARN”

    # Credential secrets consist of 2 key:value pairs
    # { key:username, value:*}
    # { key:password, value:*}

    #Set the region
    $Region = “${region}”
    Write-Log “secrets region : $Region”

    #Get the secret from the region
    $secret = Get-SECSecretValue -SecretId $SecretARN -Region $Region
    Write-Log “secrets secret : $secret”
    Write-Log "Secrets Error : $error[0]"

    #secret_key1- is the key that you want to get the value for password
    $pword = $secret.SecretString | ConvertFrom-Json | Select-Object -ExpandProperty “${secret_key1}”
    Write-Log “secrets pword : $pword”

    #secret_key2- is the key that you want to get the value for username
    $uname = $secret.SecretString | ConvertFrom-Json | Select-Object -ExpandProperty “${secret_key2}”
    Write-Log “secrets uname : $uname”

    # Create Active Directory formatted user name from supplied variables
    $username = ($domain + "\" + $uname)
    Write-Log “set username : $username”

    $password = ConvertTo-SecureString $pword -AsPlainText -Force
    Write-Log “set password : $password”

    # Create a PScrednetials Powershell object to use within our script as needed.
    $AdminCredentials = New-Object System.Management.Automation.PSCredential ("$username", $password)
    Write-Log “admin credentials : $AdminCredentials”

    #Add computer to domain
    Add-Computer -DomainName $domain -Credential $AdminCredentials
    Write-Log “add computer : $(Get-Date)”

    #reboot to make it go!
    Write-Log “reboot: $(Get-Date)”
    Restart-Computer
    Replace-Status “2"
  }

  #If case is 2, then replace with 3 and exit
  '2'{
    Replace-Status “3"
    Exit
  }

  #If case if 3, exit
  '3' {
    Exit
  }
}

</powershell>
<persist>true</persist>
