variable "region"{}
variable "availability_zone" {
  type="list"
}

variable "ami" {}

variable "key_name" {}
variable "Name" {}

variable "subnet"{
  type="list"
}

variable "instance_type"{}

variable "tags" {
  type = "map"
  default = {}
}

variable "ebs_optimized"{}


variable "security_groups"{
}

variable "ec2_count"{}

variable "instance_profile"{}

variable "ad_secret"{}
variable "ad_secret_key1"{}
variable "ad_secret_key2"{}
variable "ad_domain"{}
variable "ad_dns1"{}
variable "ad_dns2"{}
variable "ad_aws_toolkit"{}
