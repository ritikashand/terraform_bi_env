#Creating the ec2 instance for rdgw server

data "template_file" "ecs_user_data" {
  template = "${file("${path.module}/JoinDomain.tpl")}"
  vars {
    name                = "${count.index}"
    region              = "${var.region}"
    secret              = "${var.ad_secret}"
    secret_key1         = "${var.ad_secret_key1}"
    secret_key2         = "${var.ad_secret_key2}"
    aws_toolkit         = "${var.ad_aws_toolkit}"
    domain              = "${var.ad_domain}"
    dns1                = "${var.ad_dns1}"
    dns2                = "${var.ad_dns2}"
  }
}

resource "aws_instance" "ec2_instance" {
  count                  = "${var.ec2_count}"
  #pulling the subnet ID at that count
  subnet_id              = "${var.subnet[count.index]}"
  #pulling the az at that count
  availability_zone      = "${var.availability_zone[count.index]}"
  key_name               = "${var.key_name}"
  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  source_dest_check      = false
  vpc_security_group_ids = ["${split(",",var.security_groups)}"]
  iam_instance_profile      = "${var.instance_profile}"
  ebs_optimized          = "${var.ebs_optimized}"
  tags = "${merge(var.tags,map("Name","${var.Name}_${count.index}"))}"
  #user_data = "${file("JoinDomain.ps1")}"
  #user_data = "${file("${path.module}/JoinDomain.ps1")}"
  user_data = "${data.template_file.ecs_user_data.rendered}"

}


/*
provisioner "file" {
    source      = "JoinDomain.ps1"
    destination = "/JoinDomain.ps1"
  }

  provisioner "remote-exec" {
    connection {
    type = "winrm"
  }
    inline = [
      "powershell.exe JoinDomain.ps1"
    ]
  }
}
*/
