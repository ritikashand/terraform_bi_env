
#this is used for load Balancers, change according to the instances that
#the load_balancer needs
output "instance_id"{
  //gets instanece id for the 1st instance
  //value= "${aws_instance.ec2_instance.*.id[1]}"
  #of all instances using count
  value= "${aws_instance.ec2_instance.*.id}"
}
