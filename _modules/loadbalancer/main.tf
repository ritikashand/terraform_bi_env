
resource "aws_elb" "CLB" {
  name               = "LoadBalacer-RDGW-Environment"
  tags = "${merge(var.tags,map("Name","CLB_${var.Name}"))}"

  listener {
    instance_port     = 443
    instance_protocol = "tcp"
    lb_port           = 443
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "TCP:443"
    interval            = 30
  }

  subnets                   =  "${var.subnets}"
  security_groups           = ["${var.security_groups}","${var.source_security_group}"]
  instances                 = ["${var.instance_id}"]
  cross_zone_load_balancing = "${var.lb_cross_zone_load_balancing}"
  idle_timeout              = "${var.lb_idle_timeout}"
  connection_draining       = "${var.lb_connection_draining}"
  connection_draining_timeout = "${var.lb_connection_draining_timeout}"
  internal                  = "${var.lb_internal}"
  #source_security_group_id = "${var.source_security_group}" }
  #security_groups = ["${split(",",var.security_groups)}"]

  #subnets =  ["${var.subnet_id2}","${var.subnet_id1}","${var.subnet_id3}"]

  //internal = true

}
