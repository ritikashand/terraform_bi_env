variable "Name"{}
variable "instance_id"{
   type = "list"
}
variable "source_security_group"{}
variable "security_groups"{}

variable "subnets" {
  type="list"
}

variable "tags" {
  type = "map"
  default = {}
}

variable "lb_cross_zone_load_balancing"{}
variable "lb_idle_timeout"{}
variable "lb_connection_draining"{}
variable "lb_connection_draining_timeout"{}
variable "lb_internal"{}
