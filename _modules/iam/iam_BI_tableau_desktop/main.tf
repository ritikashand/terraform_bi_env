resource "aws_iam_role" "tableau_desktop_role" {
  name               = "Auto_Tableau_Desktop_Role"
  assume_role_policy = "${data.aws_iam_policy_document.tableau-desktop-role.json}"
}

resource "aws_iam_role_policy_attachment" "attach_AWSdirectory_read_acess_policy" {
  role       = "${aws_iam_role.tableau_desktop_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSDirectoryServiceReadOnlyAccess"
}


resource "aws_iam_instance_profile" "tableau_desktop_instance_profile" {
  role = "${aws_iam_role.tableau_desktop_role.name}"
}

resource "aws_iam_role_policy_attachment" "attach_secrets_manager_policy" {
  role       = "${aws_iam_role.tableau_desktop_role.name}"
  policy_arn = "arn:aws:iam::081417463982:policy/GetSecretsManagerValue"
}
