                                      ##############Creating roles##############


                ####EMR Service role####
resource "aws_iam_role" "AmazonElasticMapReduceRole" {
  name               = "auto_emr_role"
  #assume_role_policy = "${file("${path.module}/policies/emr-assume-role.json")}"
  assume_role_policy = "${data.aws_iam_policy_document.emr-assume-role.json}"

}

                ####EMR EC2 role####
resource "aws_iam_role" "AmazonElasticMapReduceforEC2Role" {
  name               = "auto_emr_ec2_role"
  #assume_role_policy = "${file("${path.module}/policies/ec2-assume-role.json")}"
  assume_role_policy = "${data.aws_iam_policy_document.emr-ec2-assume-role.json}"
}

                ####Autoscaling role####
resource "aws_iam_role" "AmazonElasticMapReduceforAutoScalingRole" {
  name               = "auto_emr_autoscale_role"
  #assume_role_policy = "${file("${path.module}/policies/emr-autoscale-assume-role.json")}"
  assume_role_policy = "${data.aws_iam_policy_document.emr-autoscale-assume-role.json}"
}


                  ##############Attaching policies to Service Role##############


             ####Attached EMR Managed Policy####
resource "aws_iam_role_policy_attachment" "attach_emr_map_reduce_policy" {
role       = "${aws_iam_role.AmazonElasticMapReduceRole.name}"
policy_arn = "${var.emr_map_reduce_policy_arn}"
}


           ####Attach KMS Disk Encryption Policy####
resource "aws_iam_role_policy" "attach_emr_kms_encryption_policy" {
  role               = "${aws_iam_role.AmazonElasticMapReduceRole.name}"
  #policy             = "${file("${path.module}/policies/emr-kms-disk-encryption.json")}"
  policy             = "${data.aws_iam_policy_document.emr-kms-disk-encryption.json}"

}

        ####Attach EMR Map Reduce Full Access Policy####
resource "aws_iam_role_policy" "attach_emr_full_access_policy" {
  role               = "${aws_iam_role.AmazonElasticMapReduceRole.name}"
  #policy             = "${file("${path.module}/policies/emr-fullaccess.json")}"
  policy             = "${data.aws_iam_policy_document.emr-fullaccess-mapreduce.json}"
}


                  ##############Attaching policies to EMR EC2 role##############


                ####Attached EMR Managed Policy####
resource "aws_iam_role_policy_attachment" "attach_ec2_map_reduce_policy" {
  role       = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
  policy_arn = "${var.ec2_map_reduce_policy_arn}"
}

               ####Attach KMS Disk Encryption Policy####
resource "aws_iam_role_policy" "attach_ec2_kms_encryption_policy" {
  role               = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
  #policy             = "${file("${path.module}/policies/emr-kms-disk-encryption.json")}"
  policy             = "${data.aws_iam_policy_document.emr-kms-disk-encryption.json}"
}


             ####Attach EMR Map Reduce Full Access Policy####
resource "aws_iam_role_policy" "attach_ec2_full_access_policy" {
  role               = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
  #policy             = "${file("${path.module}/policies/emr-fullaccess.json")}"
  policy             = "${data.aws_iam_policy_document.emr-fullaccess-mapreduce.json}"
}


              ####Attach EMR Presto Access Policy####
resource "aws_iam_role_policy" "attach_ec2_presto_access_policy" {
  role               = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
  #policy             = "${file("${path.module}/policies/emr-fullaccess.json")}"
  policy             = "${data.aws_iam_policy_document.presto-emr-access.json}"
}



            ##############Attaching policies to EMR Autoscale role##############


              ####Attached EMR Managed Policy####
resource "aws_iam_role_policy_attachment" "attach_autoscale_map_reduce_policy" {
  role       = "${aws_iam_role.AmazonElasticMapReduceforAutoScalingRole.name}"
  policy_arn = "${var.autoscale_map_reduce_policy_arn}"
}




                         ##############Create EC2 Instance Profile##############
resource "aws_iam_instance_profile" "auto_emr_ec2_instance_profile" {
  name = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
  role = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
}
