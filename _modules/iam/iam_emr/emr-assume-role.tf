data "aws_iam_policy_document" "emr-assume-role" {
  statement {
      actions =["sts:AssumeRole",]
      principals {
        type="Service"
        identifiers=["elasticmapreduce.amazonaws.com"]
      }
      effect= "Allow"
      sid= ""
    }
}
