output "emr_service_role_id" {
  value = "${aws_iam_role.AmazonElasticMapReduceRole.id}"
}

output "emr_service_role_arn"{
  value ="${aws_iam_role.AmazonElasticMapReduceRole.arn}"
}

output "emr_autoscale_role_arn"{
  value ="${aws_iam_role.AmazonElasticMapReduceforAutoScalingRole.arn}"
}

output "emr_ec2_instance_profile_arn"{
  value ="${aws_iam_instance_profile.auto_emr_ec2_instance_profile.arn}"
}
