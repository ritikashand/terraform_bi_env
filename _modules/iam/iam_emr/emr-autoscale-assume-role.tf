data "aws_iam_policy_document" "emr-autoscale-assume-role" {
  statement {
      actions =["sts:AssumeRole",]
      principals {
        type="Service"
        identifiers=["ec2.amazonaws.com"]
      }
      effect= "Allow"
    }
}
