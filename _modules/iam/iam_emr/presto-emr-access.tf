data "aws_iam_policy_document" "presto-emr-access" {
   statement {
      effect= "Allow"
      resources=["*",]
      actions = [
                "cloudwatch:*",
                "dynamodb:*",
                "ec2:Describe*",
                "elasticmapreduce:Describe*",
                "elasticmapreduce:ListBootstrapActions",
                "elasticmapreduce:ListClusters",
                "elasticmapreduce:ListInstanceGroups",
                "elasticmapreduce:ListInstances",
                "elasticmapreduce:ListSteps",
                "rds:Describe*",
                "s3:*",
                "sdb:*",
                "sns:*",
                "sqs:*",
            ]
        }
         statement {
            sid = "VisualEditor3"
            effect= "Allow"
            actions =  [
                "kms:ImportKeyMaterial",
                "kms:Decrypt",
                "kms:ListKeyPolicies",
                "kms:ListRetirableGrants",
                "kms:GetKeyPolicy",
                "kms:GenerateDataKeyWithoutPlaintext",
                "kms:ListResourceTags",
                "kms:DeleteImportedKeyMaterial",
                "kms:ListGrants",
                "kms:GetParametersForImport",
                "kms:TagResource",
                "kms:Encrypt",
                "kms:GetKeyRotationStatus",
                "kms:DescribeKey"
            ]
           resources= [
                "arn:aws:kms:us-east-1:081417463982:alias/emr-presto",
                "arn:aws:kms:us-east-1:081417463982:alias/bitools",
                "arn:aws:kms:us-east-1:081417463982:key/8af8bb14-d5bb-4bfe-b61c-1afd726ae23f",
                "arn:aws:kms:us-east-1:081417463982:key/d4a59497-1530-4c97-b9df-a8a39bbcf486"
            ]
        }
        statement {
            sid = "VisualEditor4",
            effect= "Allow",
            actions = [
                "kms:ListKeys",
                "kms:GenerateRandom",
                "kms:ListAliases",
                "kms:GenerateDataKey",
                "kms:ReEncryptTo",
                "kms:ReEncryptFrom"
            ]
            resources= ["*",]
        }
}
