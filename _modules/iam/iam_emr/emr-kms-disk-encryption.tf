data "aws_iam_policy_document" "emr-kms-disk-encryption" {
    statement {
       sid = "EmrDiskEncryptionPolicy"
       effect= "Allow"
       actions =  [
         "ds:Check*",
         "ds:Describe*",
         "ds:Get*",
         "ds:List*",
         "ds:Verify*",
         "ec2:DescribeNetworkInterfaces",
         "ec2:DescribeSubnets",
         "ec2:DescribeVpcs",
         "sns:ListTopics",
         "sns:GetTopicAttributes",
         "sns:ListSubscriptions",
         "sns:ListSubscriptionsByTopic"
              ]
       resources= ["*",]
          }
}
