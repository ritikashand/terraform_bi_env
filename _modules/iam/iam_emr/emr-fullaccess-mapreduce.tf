data "aws_iam_policy_document" "emr-fullaccess-mapreduce" {
  statement {
     sid = "VisualEditor0"
     effect= "Allow"
     actions =["elasticmapreduce:*"]
     resources= ["*",]
   }
}
