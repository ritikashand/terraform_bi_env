output "emr_cluster_name"{
   value           = "${var.name}"
 }

output "emr_release_label"{
   value  = "${var.release_label}"
 }

output "emr_applications"{
    value   = "${var.applications}"
 }

output "emr_custom_ami_id"{
   value  = "${data.aws_ami.image.id}"
}
