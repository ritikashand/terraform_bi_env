variable "name" {}

variable "vpc_id" {}

variable "subnet_id" {}

variable "release_label" {}

variable "ebs_root_volume_size" {
}

variable "autoscaling_role"{}

variable "service_role"{}

variable "instance_profile"{}

variable "security_configuration"{}

variable "emr_managed_master_security_group"{}

variable "emr_managed_slave_security_group"{}

variable "applications" {
 type    = "list"
}

variable "key_name" {}

variable "tags" {
  type = "map"
  default = {}
}

variable "ami_filter_tag"{
  type="list"
  default=[]
}

variable "log_uri"{}

variable "instance_groups" {
  default = [
    {
      name           = "MasterInstanceGroup"
      instance_role  = "MASTER"
      instance_type  = "m4.large"
      instance_count = 1
    },
    {
     name           = "TaskInstanceGroup"
     instance_role  = "TASK"
     instance_type  = "m4.large"
     instance_count = 1
     autoscaling_policy = <<EOF
{
 "Constraints": {
   "MinCapacity": 2,
   "MaxCapacity": 12
 },
 "Rules": [
 {
   "Name": "ScaleOutMemoryPercentage",
   "Description": "Scale out if YARNMemoryAvailablePercentage is less than 15",
   "Action": {
     "SimpleScalingPolicyConfiguration": {
       "AdjustmentType": "CHANGE_IN_CAPACITY",
       "ScalingAdjustment": 1,
       "CoolDown": 300
     }
   },
   "Trigger": {
     "CloudWatchAlarmDefinition": {
       "ComparisonOperator": "LESS_THAN",
       "EvaluationPeriods": 1,
       "MetricName": "YARNMemoryAvailablePercentage",
       "Namespace": "AWS/ElasticMapReduce",
       "Period": 300,
       "Statistic": "AVERAGE",
       "Threshold": 15.0,
       "Unit": "PERCENT"
      }
    }
  }
 ]
}
EOF
   },
    {
      name           = "CoreInstanceGroup"
      instance_role  = "CORE"
      instance_type  = "m4.large"
      instance_count = "1"
      autoscaling_policy = <<EOF
{
  "Constraints": {
    "MinCapacity": 2,
    "MaxCapacity": 12
  },
  "Rules": [
  {
    "Name": "ScaleOutMemoryPercentage",
    "Description": "Scale out if YARNMemoryAvailablePercentage is less than 15",
    "Action": {
      "SimpleScalingPolicyConfiguration": {
        "AdjustmentType": "CHANGE_IN_CAPACITY",
        "ScalingAdjustment": 1,
        "CoolDown": 300
      }
    },
    "Trigger": {
      "CloudWatchAlarmDefinition": {
        "ComparisonOperator": "LESS_THAN",
        "EvaluationPeriods": 1,
        "MetricName": "YARNMemoryAvailablePercentage",
        "Namespace": "AWS/ElasticMapReduce",
        "Period": 300,
        "Statistic": "AVERAGE",
        "Threshold": 15.0,
        "Unit": "PERCENT"
       }
     }
   }
  ]
}
EOF
    },
  ]
  type = "list"
}

variable "configurations" {}

variable "hadoop_jar_step_args" {
  type = "list"
}

variable "step_action" {}

variable "step_name" {}

variable "hadoop_jar_file" {}

# Uncomment the following code when enabling kerberos on the Cluster:
/*
variable "ad_domain_join_password" {}
variable "ad_domain_join_user" {}
variable "cross_realm_trust_principal_password" {}
variable "kdc_admin_password" {}
variable "realm" {}
*/
