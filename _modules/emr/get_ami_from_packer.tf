data "aws_ami" "image" {
 most_recent = true
 owners = ["self"]
 filter {
   name= "tag:Name"
   values=["${var.ami_filter_tag}"]
 }
}
