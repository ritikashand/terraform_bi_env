#Creates emr cluster


resource "aws_emr_cluster" "cluster" {
  name           = "${var.name}"
  release_label  = "${var.release_label}"
  applications   = "${var.applications}"
  custom_ami_id  = "${data.aws_ami.image.id}"
  autoscaling_role = "${var.autoscaling_role}"
  service_role = "${var.service_role}"
  instance_group = "${var.instance_groups}"
  security_configuration="${var.security_configuration}"
  log_uri= "${var.log_uri}"
  ebs_root_volume_size = "${var.ebs_root_volume_size}"
  configurations= "${var.configurations}"
  step {
    action_on_failure = "${var.step_action}"
    name   = "${var.step_name}"

    hadoop_jar_step {
      jar  = "${var.hadoop_jar_file}"
      args = "${var.hadoop_jar_step_args}"
    }
  }



 ec2_attributes {
    key_name                          = "${var.key_name}"
    subnet_id                         = "${var.subnet_id}"
    emr_managed_master_security_group = "${var.emr_managed_master_security_group}"
    emr_managed_slave_security_group  = "${var.emr_managed_slave_security_group}"
    instance_profile                  = "${var.instance_profile}"
  }


  # Uncomment the following code when enabling kerberos on the Cluster:
  /*
  kerberos_attributes{
    ad_domain_join_password= "${var.ad_domain_join_password}"
    ad_domain_join_user ="${var.ad_domain_join_user}"
    cross_realm_trust_principal_password ="${var.cross_realm_trust_principal_password}"
    kdc_admin_password  = "${var.kdc_admin_password}"
    realm = "${var.realm}"
  }
  */

  tags = "${merge(var.tags,map("Name","${var.name}"))}"

 }
