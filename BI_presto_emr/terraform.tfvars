
Name          = "Presto_EMR_Cluster-05172018_11p"
vpc_id        = "vpc-25046c5e"
subnet_id     = "subnet-a1f445ae"
release_label = "emr-5.12.1"
applications  = ["Presto","Hive","Hue","Pig"]
#applications = ["Spark","Hadoop","Pig","Hue","Zeppelin","Hive", "HCatalog","HBase","Presto","Tez","ZooKeeper",
#                "Sqoop","Phoenix","Flink","Mahout","Oozie","Livy","Ganglia","MXNet"]

ebs_root_volume_size="20"
key_name     = "jo567j"

tags={
  Project     = "Presto-EMR-Cluster"
  Environment = "Mac-SB-Presto-EMR-Cluster"
  Department  = "MAC"
  Requestor   = "ra256j"
}

cidr_block_master = ["0.0.0.0/0"]
cidr_block_slave = ["0.0.0.0/0"]

log_uri="s3://aws-logs-081417463982-us-east-1/elasticmapreduce/"

ami_filter_tag=["EMR-SB-Custom-Ami-04302018-5p"]


iam_key_users_permissions_identifiers = [
        "arn:aws:iam::081417463982:root"
        ]

iam_users_key_administrators_access= [
        "arn:aws:iam::081417463982:user/sm856a"
        ]
iam_key_users = [
        "arn:aws:iam::081417463982:role/auto_emr_ec2_role",
        "arn:aws:iam::081417463982:user/rs256x",
        "arn:aws:iam::081417463982:role/auto_emr_role",
        "arn:aws:iam::081417463982:user/ra567j",
        "arn:aws:iam::081417463982:user/sm856a"
        ]
grant_resource_for_key=[
        "arn:aws:iam::081417463982:role/auto_emr_ec2_role",
        "arn:aws:iam::081417463982:user/rs256x",
        "arn:aws:iam::081417463982:role/auto_emr_role",
        "arn:aws:iam::081417463982:user/ra567j",
        "arn:aws:iam::081417463982:user/sm856a"
        ]

emr_kms_key_alias_name = "alias/auto-emr/emr_kms_key_05072018_4p"

hive-s3-sse-kms-key-id= "15d49ea7-092d-4fe0-824d-4498ae280a2e"

hive-s3-max-connections= "1000"

emr_map_reduce_policy_arn="arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceRole"

ec2_map_reduce_policy_arn="arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceforEC2Role"

autoscale_map_reduce_policy_arn="arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceforAutoScalingRole"

hadoop_jar_step_args=["hive-script", "--run-hive-script", "--args", "-f",  "s3://ra-emr-presto-step-job/input/create_table.q"]

step_action="CONTINUE"

step_name="Setup Hadoop Debugging"

hadoop_jar_file="command-runner.jar"


# Uncomment the following code when enabling kerberos on the Cluster:
/*
ad_domain_join_password = "MyClusterKDCAdmi"
ad_domain_join_user ="ADUserLogonName"
cross_realm_trust_principal_password = "MatchADTrustPwd"
kdc_admin_password  = "MyClusterKDCAdminPwd"
realm = "MACDEV.ATT.COM"
*/
