variable "tags" {
  type = "map"
  default = {}
}

variable "iam_key_users_permissions_identifiers"{
  type = "list"
  default=[]
}

variable "iam_users_key_administrators_access"{
  type = "list"
  default=[]
}

variable "iam_key_users"{
  type = "list"
  default=[]
}

variable "grant_resource_for_key"{
  type = "list"
  default=[]
}

variable "emr_kms_key_alias_name" {
}
