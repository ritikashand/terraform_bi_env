data "aws_iam_policy_document" "emr_kms_policy" {
  statement {
      sid = "Enable IAM User Permissions"
      effect= "Allow"
      principals {
        type = "AWS"
			  identifiers = ["${var.iam_key_users_permissions_identifiers}"]
      }
      actions= ["kms:*",]
      resources= ["*",]
    }
  statement {
      sid = "Allow access for Key Administrators"
      effect= "Allow"
      principals {
        type = "AWS"
			  identifiers = ["${var.iam_users_key_administrators_access}"]
      }
      actions= [
        "kms:Create*",
        "kms:Describe*",
        "kms:Enable*",
        "kms:List*",
        "kms:Put*",
        "kms:Update*",
        "kms:Revoke*",
        "kms:Disable*",
        "kms:Get*",
        "kms:Delete*",
        "kms:TagResource",
        "kms:UntagResource",
        "kms:ScheduleKeyDeletion",
        "kms:CancelKeyDeletion"
      ]
      resources=  ["*",]
    }
  statement {
      sid = "Allow use of the key"
      effect= "Allow"
      principals {
        type = "AWS"
			  identifiers = ["${var.iam_key_users}"]
      }
      actions= [
        "kms:Encrypt",
        "kms:Decrypt",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:DescribeKey",
        "kms:CreateGrant",
        "kms:GenerateDataKeyWithoutPlaintext"
      ]
      resources= ["*",]
    }
  statement {
      sid= "Allow attachment of persistent resources"
      effect= "Allow"
      principals {
        type = "AWS"
       	identifiers = ["${var.grant_resource_for_key}"]
      }
      actions= [
        "kms:CreateGrant",
        "kms:ListGrants",
        "kms:RevokeGrant"
      ]
      resources= ["*",]
      condition {
          test = "Bool"
          variable = "kms:GrantIsForAWSResource"
          values = ["true",]
      }
    }
}
