variable "Name" {}

variable "vpc_id" {}

variable "subnet_id" {}

variable "release_label" {}

variable "applications" {
 type    = "list"
}

variable "cidr_block_master"{
  type = "list"
}
variable "cidr_block_slave"{
  type = "list"
}
variable "ebs_root_volume_size" {
}

variable "key_name" {}

variable "tags" {
  type = "map"
  default = {}
}

variable "ami_filter_tag"{
  type="list"
  default=[]
}

variable "log_uri"{}

variable "iam_key_users_permissions_identifiers"{
  type = "list"
  default=[]
}

variable "iam_users_key_administrators_access"{
  type = "list"
  default=[]
}

variable "iam_key_users"{
  type = "list"
  default=[]
}

variable "grant_resource_for_key"{
  type = "list"
  default=[]
}

variable "emr_kms_key_alias_name" {}

variable "hive-s3-sse-kms-key-id" {}

variable "hive-s3-max-connections" {}

variable "emr_map_reduce_policy_arn" {}

variable "ec2_map_reduce_policy_arn" {}

variable "autoscale_map_reduce_policy_arn" {}

variable "hadoop_jar_step_args" {
  type = "list"
}

variable "step_action" {}

variable "step_name" {}

variable "hadoop_jar_file" {}

# Uncomment the following code when enabling kerberos on the Cluster:
/*
variable "ad_domain_join_password" {}
variable "ad_domain_join_user" {}
variable "cross_realm_trust_principal_password" {}
variable "kdc_admin_password" {}
variable "realm" {}
*/
