data "template_file" "presto_configurations" {
  template = "${file("configurations.json")}"
  vars {
        hive-s3-sse-kms-key-id = "${var.hive-s3-sse-kms-key-id}"
        hive-s3-max-connections = "${var.hive-s3-max-connections}"
    }

}

module "iam" {
  source = "../_modules/iam/iam_emr"
  emr_map_reduce_policy_arn="${var.emr_map_reduce_policy_arn}"
  ec2_map_reduce_policy_arn="${var.ec2_map_reduce_policy_arn}"
  autoscale_map_reduce_policy_arn="${var.autoscale_map_reduce_policy_arn}"
}


module "security_group" {
  source = "./security_groups"
  Name          = "${var.Name}"
  vpc_id        = "${var.vpc_id}"
  subnet_id     = "${var.subnet_id}"
  tags          = "${var.tags}"
  cidr_block_master = "${var.cidr_block_master}"
  cidr_block_slave  = "${var.cidr_block_slave}"

}


module "encryption" {
  source = "./security_configuration"
  iam_key_users_permissions_identifiers= "${var.iam_key_users_permissions_identifiers}"
  iam_users_key_administrators_access= "${var.iam_users_key_administrators_access}"
  iam_key_users="${var.iam_key_users}"
  grant_resource_for_key="${var.grant_resource_for_key}"
  emr_kms_key_alias_name = "${var.emr_kms_key_alias_name}"

}


module "emr_cluster" {
  source           = "../_modules/emr"
  ami_filter_tag   = "${var.ami_filter_tag}"
  log_uri          ="${var.log_uri}"
  vpc_id           = "${var.vpc_id}"
  subnet_id        = "${var.subnet_id}"
  name             = "${var.Name}"
  ebs_root_volume_size = "${var.ebs_root_volume_size}"
  key_name         = "${var.key_name}"
  release_label    = "${var.release_label}"
  applications     = "${var.applications}"
  autoscaling_role = "${module.iam.emr_autoscale_role_arn}"
  service_role     = "${module.iam.emr_service_role_arn}"
  instance_profile = "${module.iam.emr_ec2_instance_profile_arn}"
  security_configuration="${module.encryption.emr_security_configuration}"
  emr_managed_master_security_group = "${module.security_group.master_security_group_id}"
  emr_managed_slave_security_group = "${module.security_group.slave_security_group_id}"
  configurations = "${data.template_file.presto_configurations.rendered}"
  tags           = "${var.tags}"
  step_action = "${var.step_action}"
  step_name   = "${var.step_name}"
  hadoop_jar_file  = "${var.hadoop_jar_file}"
  hadoop_jar_step_args = "${var.hadoop_jar_step_args}"
  # Uncomment the following code when enabling kerberos on the Cluster
  /*
  ad_domain_join_password= "${var.ad_domain_join_password}"
  ad_domain_join_user ="${var.ad_domain_join_user}"
  cross_realm_trust_principal_password = "${var.cross_realm_trust_principal_password}"
  kdc_admin_password  = "${var.kdc_admin_password}"
  realm = "${var.realm}"
  */
}
