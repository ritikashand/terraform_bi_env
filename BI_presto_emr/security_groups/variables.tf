variable "Name" {}

variable "vpc_id" {}

variable "subnet_id" {}

variable "tags" {
  type = "map"
  default = {}
}

variable "cidr_block_master"{
  type = "list"
}
variable "cidr_block_slave"{
  type = "list"
}
