
Name          = "Tableau_Desktop_BI_05/24/2018_11a"
vpc_id        = "vpc-73741708"
instance_type = "t2.medium"
subnet        = ["subnet-a1ac7ceb","subnet-d3cec28e"]
availability_zone = ["us-east-1a","us-east-1b"]
ami           = "ami-c38e1bbc"
key_name      = "RDGW-keypair"
ec2_count     = "2"
ebs_optimized = "false"

ad_secret= "arn:aws:secretsmanager:us-east-1:081417463982:secret:macdev/admin/public-DhNDI0"
ad_secret_key1= "Password"
ad_secret_key2= "Username"
ad_domain = "macdev.att.com"
ad_dns1 = "130.6.227.117"
ad_dns2 = "130.6.227.170"
ad_aws_toolkit = "http://sdk-for-net.amazonwebservices.com/latest/AWSToolsAndSDKForNet.msi"

cidr_block = {
  all = "0.0.0.0/0"
  rdpgw_vpc = "130.6.228.0/24"
  }

tags={
  Project     = "Auto-BI-Tableau_Desktop-Environment"
  Environment = "SB-BI-Environment"
  Department  = "MAC"
  Requestor   = "rs256x"
}
