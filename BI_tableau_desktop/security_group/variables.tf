variable "Name" {}
#variable "sg_Name" {}

variable "vpc_id" {}
#variable "sg_vpc_id" {}

variable "cidr_block" {
  type="map"
}

variable "tags" {
  type = "map"
  default = {}
}
