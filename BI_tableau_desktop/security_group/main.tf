#
## Create Security Group for Master Node
#

resource "aws_security_group" "tableau_desktop_security_group" {
  vpc_id                 = "${var.vpc_id}"
  description            = "Security Group"
  revoke_rules_on_delete = true


  ingress {
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"rdpgw_vpc")}"]
    description = "RDP from Remote Desktop VPC"
  }

  ingress {
    from_port   = 5985
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"rdpgw_vpc")}"]
    description = "Winrm from Remote Desktop VPC"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"rdpgw_vpc")}"]
    description = "SSL from Remote Desktop VPC"
  }
  ingress {
    from_port   = 5985
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks = ["65.74.232.0/32"]
    description = "Ritikas Desktop - WinRM"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${lookup(var.cidr_block,"all")}"]
  }

  tags = "${merge(var.tags,map("Name","SG_${var.Name}_Tableau_Desktop"))}"

}
