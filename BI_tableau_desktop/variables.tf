variable "region"{
  default="us-east-1"
}

variable "vpc_id" {}

variable "ami" {}

variable "Name" {}

variable "availability_zone"{
  type ="list"
}
variable "subnet"{
  type="list"
}

variable "cidr_block" {
  type="map"
}

variable "instance_type"{}

variable "tags" {
  type = "map"
  default = {}
}

variable "ebs_optimized"{}

variable "ec2_count"{}

variable "key_name" {}


variable "ad_secret"{}

variable "ad_secret_key1"{}

variable "ad_secret_key2"{}

variable "ad_domain"{}

variable "ad_dns1"{}

variable "ad_dns2"{}

variable "ad_aws_toolkit"{}
