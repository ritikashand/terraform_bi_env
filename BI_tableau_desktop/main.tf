module "iam" {
  source = "../_modules/iam/iam_BI_tableau_desktop"
}

#Security group for tableau_desktop instance
module "security_group" {
  source = "./security_group"
  Name          = "${var.Name}"
  vpc_id        = "${var.vpc_id}"
  cidr_block    = "${var.cidr_block}"
  tags          = "${var.tags}"

}

#
#Create 2 ec2 instances for tableau_desktop in two different subnets and az
#
#change spelling
module "ec2_instance" {
  source        = "../_modules/ec2"
  Name                   = "${var.Name}"
  ami                    = "${var.ami}"
  key_name               = "${var.key_name}"
  ec2_count              = "${var.ec2_count}"
  instance_type          = "${var.instance_type}"
  subnet                 = "${var.subnet}"
  availability_zone      = "${var.availability_zone}"
  security_groups        = "${module.security_group.tableau_desktop_security_group_id}"
  ebs_optimized          = "${var.ebs_optimized}"
  instance_profile       = "${module.iam.instance_profile_arn}"
  tags                   = "${var.tags}"
  region                 = "${var.region}"
  ad_secret              = "${var.ad_secret}"
  ad_secret_key1         = "${var.ad_secret_key1}"
  ad_secret_key2         = "${var.ad_secret_key2}"
  ad_aws_toolkit         = "${var.ad_aws_toolkit}"
  ad_domain              = "${var.ad_domain}"
  ad_dns1                = "${var.ad_dns1}"
  ad_dns2                = "${var.ad_dns2}"

}
