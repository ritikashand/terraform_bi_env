variable "peer_owner_id" {}

variable "vpc_peer_id" {}

variable "vpc_id" {}

variable "peer_region" {}

variable "Name"{}
#variable "accepter" {}

#variable "requester" {}

variable "tags" {
  type = "map"
  default = {}
}
