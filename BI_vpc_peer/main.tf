
module "vpc_peer" {

  source        = "../_modules/vpc_peer"
  Name          = "${var.Name}"
  peer_owner_id = "${var.peer_owner_id}"
  vpc_peer_id   = "${var.vpc_peer_id}"
  vpc_id        = "${var.vpc_id}"
  peer_region   = "${var.peer_region}"
  tags          = "${var.tags}"
}
