
Name          =  "VPC_Peer_BI_Environment_05/17/2018_11a"
peer_owner_id =  "594349222397"
vpc_peer_id   =  "vpc-ba65b3c2"
vpc_id        =  "vpc-92cdd0ea"
peer_region   =  "us-east-1"


tags={
  Project     = "Auto-BI-Environment"
  Environment = "SB-BI-Environment"
  Department  = "MAC"
  Requestor   = "rishand"
  group_name  = "Auto-RDGW-Environment"
  Owner       = "rs256x"
}
