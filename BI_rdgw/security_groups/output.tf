output "rdgw_server_security_group_id" {
  value = "${aws_security_group.security_group_rdgw_server.id}"
}

output "source_lb_security_group_id" {
  value = "${aws_security_group.source_security_group_load_balacer.id}"
}

output "lb_security_group_id" {
  value = "${aws_security_group.security_group_load_balacer.id}"
}
