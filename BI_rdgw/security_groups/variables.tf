variable "Name" {}

variable "vpc_id" {}

variable "tags" {
  type = "map"
  default = {}
}

variable "cidr_block" {
  type="map"
}
