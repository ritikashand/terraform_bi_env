#
## Create Security Group for Ec2 Instance
#

resource "aws_security_group" "security_group_rdgw_server" {
  vpc_id                 = "${var.vpc_id}"
  description            = "Security Group"
  revoke_rules_on_delete = true

  ingress {
    from_port   = 8
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["${lookup(var.cidr_block,"dev")}"]
    description = "ICMP from Dev dv-mac-main"
  }

  ingress {
    from_port   = 135
    to_port     = 135
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"dev")}"]
    description = "NetBIOS from VPC with Directory Service"
  }

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "udp"
    cidr_blocks = ["${lookup(var.cidr_block,"dev")}"]
    description = "DNS to Dev VPC with Directory Service"
  }

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"dev")}"]
    description = "DNS to Dev VPC with Directory Service"
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"sandbox")}"]
    description = "RDP access from this VPC"
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"enclave1")}"]
    description = "RDP from VPN"
  }

  ingress {
    from_port   = 445
    to_port     = 445
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"dev")}"]
    description = "NetBIOS access for AD from VPC with Directory"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"dev")}"]
    description = "SSL access from this vpc"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"enclave1")}"]
    description = "SSL from vpn"
  }

  ingress {
    from_port   = 5985
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks = ["${lookup(var.cidr_block,"sandbox")}"]
    description = "WinRM access from VPC "
  }


  ingress {
    from_port   = 5985
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks = ["65.74.232.0/32"]
    description = "Ritikas Desktop - WinRM"
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${lookup(var.cidr_block,"all")}"]
  }

  tags = "${merge(var.tags,map("Name","SG_RDGWServer_${var.Name}"))}"

}

resource "aws_security_group" "source_security_group_load_balacer" {
  vpc_id                 = "${var.vpc_id}"
  description            = "Security Group for Load Balancer-1"
  revoke_rules_on_delete = true

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${lookup(var.cidr_block,"all")}"]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${lookup(var.cidr_block,"all")}"]
  }
  tags = "${merge(var.tags,map("Name","SG_LB1_${var.Name}"))}"
}


resource "aws_security_group" "security_group_load_balacer" {
    vpc_id                 = "${var.vpc_id}"
    description            = "Security Group for Load Balancer-2"
    revoke_rules_on_delete = true

    egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["${lookup(var.cidr_block,"all")}"]
    }

    ingress {
      from_port   = 0
      to_port     = 0
      protocol = "-1"
      cidr_blocks  = ["${lookup(var.cidr_block,"sandbox")}"]
    }
    tags = "${merge(var.tags,map("Name","SG_LB2_${var.Name}"))}"
  }
