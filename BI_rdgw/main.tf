module "iam" {
  source = "../_modules/iam/iam_BI"
}

#Security group for tableau_desktop instance
module "security_group" {
  source = "./security_groups"
  Name          = "${var.Name}"
  vpc_id        = "${var.vpc_id}"
  cidr_block    = "${var.cidr_block}"
  tags          = "${var.tags}"
}

#
#Create 2 ec2 instances for rdgw server in two different subnets and az
#
module "ec2-instance" {
  source                 = "../_modules/ec2"
  Name                   = "RDGWServer_${var.Name}"
  ami                    = "${var.ami_rdgw_server}"
  key_name               = "${var.key_name}"
  #if multiple instances in same subnet?
  ec2_count              = "${var.ec2_count}"
  instance_type          = "${var.instance_type_rdgw}"
  #subnet_id              = "${var.subnet_id1}"
  subnet                 = "${var.subnet}"
  availability_zone      = "${var.availability_zone}"
  security_groups        = "${module.security_group.rdgw_server_security_group_id}"
  ebs_optimized          = "${var.ebs_optimized_rdgw}"
  instance_profile       = "${module.iam.rdgw_instance_profile_arn}"
  tags                   = "${var.tags}"
  region                 = "${var.region}"
  ad_secret              = "${var.ad_secret}"
  ad_secret_key1         = "${var.ad_secret_key1}"
  ad_secret_key2         = "${var.ad_secret_key2}"
  ad_aws_toolkit         = "${var.ad_aws_toolkit}"
  ad_domain              = "${var.ad_domain}"
  ad_dns1                = "${var.ad_dns1}"
  ad_dns2                = "${var.ad_dns2}"
}


/*
module "load_balancer"{
  source                = "../_modules/loadbalancer"
  Name                  = "${var.Name}"
  instance_id           = "${module.ec2-instance.instance_id}"
  source_security_group = "${module.security_group.source_lb_security_group_id}"
  security_groups       = "${module.security_group.lb_security_group_id}"
  #subnets =  ["${var.subnet_id2}","${var.subnet_id1}","${var.subnet_id3}"]
  subnets               = "${var.subnet}"
  lb_cross_zone_load_balancing = "${var.lb_cross_zone_load_balancing}"
  lb_idle_timeout       = "${var.lb_idle_timeout}"
  lb_connection_draining = "${var.lb_connection_draining}"
  lb_connection_draining_timeout = "${var.lb_connection_draining_timeout}"
  lb_internal           = "${var.lb_internal}"
  tags                  = "${var.tags}"
}
*/
