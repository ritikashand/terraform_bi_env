variable "region"{
  default="us-east-1"
}
variable "vpc_id" {}

variable "Name" {}

variable "availability_zone"{
  type ="list"
}
variable "subnet"{
  type="list"
}

variable "cidr_block" {
  type="map"
}
variable "ami_rdgw_server" {}

variable "instance_type_rdgw"{}

variable "ebs_optimized_rdgw"{}

variable "ec2_count"{}
variable "key_name" {}

variable "tags" {
  type = "map"
  default = {}
}

variable "ad_secret"{}
variable "ad_secret_key1"{}
variable "ad_secret_key2"{}
variable "ad_domain"{}
variable "ad_dns1"{}
variable "ad_dns2"{}
variable "ad_aws_toolkit"{}

variable "lb_cross_zone_load_balancing"{}
variable "lb_idle_timeout"{}
variable "lb_connection_draining"{}
variable "lb_connection_draining_timeout"{}
variable "lb_internal"{}
