Name          = "RDGW_Environment_05/17/2018_11a"
vpc_id        = "vpc-92cdd0ea"
#subnet as a list, pulling the subnet value using count
subnet        = ["subnet-dce91696","subnet-caf7dc97","subnet-890519ed"]
availability_zone = ["us-east-1a","us-east-1b","us-east-1c"]
instance_type_rdgw = "m4.large"
#custom ami
ami_rdgw_server    = "ami-1fbc3260"
#base ami
#ami_rdgw_server    = "ami-f0df538f"

key_name      = "RDGW-keypair"
ec2_count     = "2"
ebs_optimized_rdgw = "true"

cidr_block = {
  all = "0.0.0.0/0"
  dev = "130.6.227.0/24"
  sandbox = "130.6.228.0/24"
  enclave1 = "68.121.9.0/24"
  enclave2 = "68.90.109.0/24"
  }

//load_balancer variables:

lb_cross_zone_load_balancing   = "true"
lb_idle_timeout                ="60"
lb_connection_draining         = "true"
lb_connection_draining_timeout = "300"
lb_internal = "true"

ad_secret= "arn:aws:secretsmanager:us-east-1:081417463982:secret:macdev/admin/public-DhNDI0"
ad_secret_key1= "Password"
ad_secret_key2= "Username"
ad_domain = "macdev.att.com"
ad_dns1 = "130.6.227.117"
ad_dns2 = "130.6.227.170"
ad_aws_toolkit = "http://sdk-for-net.amazonwebservices.com/latest/AWSToolsAndSDKForNet.msi"

tags={
  Project     = "Auto-BI-RDGW-Environment"
  Environment = "SB-BI-Environment"
  Department  = "MAC"
  Requestor   = "rs256x"
}
